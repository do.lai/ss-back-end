'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('order_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      quantity: {
        type: Sequelize.DECIMAL
      },
      sum: {
        type: Sequelize.DECIMAL
      },
      image: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      code: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
        orderId:{
            type: Sequelize.INTEGER,
            onDelete : 'CASCADE',
            references: {
                model: 'orders',
                key: 'id',
                as: 'orderId'
            },
        },
        shopItemId:{
            type: Sequelize.INTEGER,
            references:{
                model: 'shop_item_details',
                key: 'id',
                as: 'shopItemId'
            },
        },
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('order_details');
  }
};