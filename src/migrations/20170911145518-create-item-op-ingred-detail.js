'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('item_op_ingred_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      amount: {
        type: Sequelize.DECIMAL
      },
      amount_type: {
        type: Sequelize.STRING
      },
      unit: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
        itemOptionId: {
        type: Sequelize.INTEGER,
            references: {
          model: 'item_options',
                key : 'id',
                as: 'itemOptionId'
            },
        },
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('item_op_ingred_details');
  }
};