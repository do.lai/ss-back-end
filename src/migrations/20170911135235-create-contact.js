'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('contacts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      email: {
        type: Sequelize.STRING
      },
      email2: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      name2: {
        type: Sequelize.STRING
      },
      hotline: {
        type: Sequelize.STRING
      },
      office_phone: {
        type: Sequelize.STRING
      },
      office_phone2: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('contacts');
  }
};