'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('supplier_ingred_op_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      pirce: {
        type: Sequelize.DECIMAL
      },
      unit: {
        type: Sequelize.STRING
      },
      ship_fee: {
        type: Sequelize.DECIMAL
      },
      sku_code: {
        type: Sequelize.STRING
      },
      sku_name: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      best_before: {
        type: Sequelize.DATE
      },
      expired_after: {
        type: Sequelize.DECIMAL
      },
      expired_type: {
        type: Sequelize.STRING
      },
      expired_unit: {
        type: Sequelize.STRING
      },
      origin: {
        type: Sequelize.STRING
      },
      licence_link: {
        type: Sequelize.TEXT
      },
      lisence_link2: {
        type: Sequelize.TEXT
      },
      lisence_link3: {
        type: Sequelize.TEXT
      },
      lisence_link4: {
        type: Sequelize.TEXT
      },
      lisence_link5: {
        type: Sequelize.TEXT
      },
      status: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
        ingredOptionId: {
        type: Sequelize.INTEGER,
            references: {
          model: 'ingred_options',
                key: 'id',
                as: 'ingredOptionId'
            },
        },
        supplierId: {
        type: Sequelize.INTEGER,
            references:{
          model: 'suppliers',
                key: 'id',
                as: 'supplierId'
            },
        },
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('supplier_ingred_op_details');
  }
};