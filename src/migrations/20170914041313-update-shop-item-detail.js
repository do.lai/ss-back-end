'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return [
        queryInterface.addColumn('shop_item_details' , 'shopId' ,{
          type: Sequelize.INTEGER,
            references: {
            model: 'shops',
                key: 'id',
                as: 'shopId'
            }
        }),
        queryInterface.addColumn('shop_item_details' , 'itemOptionId',{
          type: Sequelize.INTEGER,
            references:{
            model: 'item_options',
                key: 'id',
                as: 'itemOptionId'
            }
        })
    ];
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    return queryInterface.dropTable('shop_item_details')
  }
};
