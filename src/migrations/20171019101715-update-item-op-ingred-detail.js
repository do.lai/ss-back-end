'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
      return [
          queryInterface.addColumn('item_op_ingred_details' , 'ingredOptionId' ,{
              type: Sequelize.INTEGER,
              references: {
                  model: 'ingred_options',
                  key: 'id',
                  as: 'ingredOptionId'
              }
          }),
          queryInterface.addColumn('item_op_ingred_details' , 'name' ,{
              type: Sequelize.STRING,
          })
      ];
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
      return queryInterface.dropTable('item_op_ingred_details');
  }
};
