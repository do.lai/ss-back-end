'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
      return [
          queryInterface.addColumn('shop_item_details' , 'itemId' ,{
              type: Sequelize.INTEGER,
              references: {
                  model: 'items',
                  key: 'id',
                  as: 'itemId'
              }
          })
          ];
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    return queryInterface.dropTable('shop_item_details');
  }
};
