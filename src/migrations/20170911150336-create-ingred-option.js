'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('ingred_options', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      photo: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      order_count: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
        ingredientId: {
        type: Sequelize.INTEGER,
            references:{
          model: 'ingredients',
                key: 'id',
                as: 'ingredientIds'
            }
        }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('ingred_options');
  }
};