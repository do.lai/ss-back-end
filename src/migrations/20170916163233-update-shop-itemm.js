'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return [
        queryInterface.addColumn('shop_item_details' , 'itemOptionItemId' ,{
            type: Sequelize.INTEGER,
            references: {
                model: 'item_options',
                key: 'id',
                as: 'itemOptionItemId'
            }
        })
    ];
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
