'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('shop_item_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      local_item_name: {
        type: Sequelize.STRING
      },
      local_item_code: {
        type: Sequelize.STRING
      },
      local_item_desc: {
        type: Sequelize.STRING
      },
      item_price: {
        type: Sequelize.DECIMAL
      },
      item_unit: {
        type: Sequelize.STRING
      },
      local_photo: {
        type: Sequelize.STRING
      },
      local_photo2: {
        type: Sequelize.STRING
      },
      local_photo3: {
        type: Sequelize.STRING
      },
      local_photo4: {
        type: Sequelize.STRING
      },
      local_photo5: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },

    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('shop_item_details');
  }
};