'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('itemCat_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
        itemId:{
            type : Sequelize.INTEGER,
            onDelete: 'CASCADE',
            references: {
                model:'items',
                key: 'id',
                as: 'itemId'
            },
        },
        itemCatId:{
            type : Sequelize.INTEGER,
            onDelete: 'CASCADE',
            references: {
                model:'item_cats',
                key: 'id',
                as: 'itemCatId'
            },
        },
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('itemCat_details');
  }
};