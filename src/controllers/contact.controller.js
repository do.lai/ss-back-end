/**
 * Created by hoangdo on 9/14/2017.
 */

const  Contact = require('../models').contact;
module.exports = {
    create(req, res){
        return Contact
            .create({
                email: req.body.email ,
                email2: req.body.email2,
                name: req.body.name,
                name2: req.body.name2,
                hotline: req.body.hotline,
                office_phone: req.body.office_phone,
                office_phone2: req.body.office_phone2,
                latitude: req.body.latitude,
                longitude: req.body.longitude
            })
            .then(contact => res.status(201).send(contact))
            .catch(error => res.status(500).send(error));
    },
    list(req,res){
        return Contact
            .all()
            .then(contact => {
                if(contact.length === 0){
                    return res.status(404).send({
                        message: 'Array Empty'
                    })
                }
                return res.status(201).send(contact);
            })
            .catch(error => res.status(500).send(error));
    }
}