/**
 * Created by hoangdo on 10/19/2017.
 */


const ItemOpIngredDetail = require('../../models').item_op_ingred_detail;
const ItemOption = require('../../models').item_option;

module.exports ={
    create(req , res){
        return ItemOpIngredDetail
            .create({
                amount : req.body.amount,
                amount_type: req.body.amount_type,
                unit : req.body.unit,
                itemOptionId: req.body.itemOptionId,
                ingredOptionId: req.body.ingredOptionId,
                name : req.body.name
            })
            .then(itemop =>res.status(201).send(itemop))
            .catch(error => res.status(500).send(error));
    },
    getAll(req,res){
        return ItemOpIngredDetail
            .findAll({
                include: {
                    all: true
                }
            })
            .then((itemopingred) => res.status(201).send(itemopingred))
            .catch(error => res.status(500).send(error));
    }
};