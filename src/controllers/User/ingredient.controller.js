/**
 * Created by hoangdo on 9/16/2017.
 */

const IngredientUser = require('../../models').ingredient;

module.exports ={
    list(req,res){
        return IngredientUser
            .all()
            .then(ingredient => res.status(201).send(ingredient))
            .catch(error => res.status(500).send(error));
    },
    retrieve(req,res){
        return IngredientUser
            .all
            .then(ingredient => res.status(201).send(ingredient))
            .catch(error => res.status(500).send(error));
    },
    getByTypeMain(req,res){
        return IngredientUser
            .findAll({
              where :{
                  type: 'main'
              }
            })
            .then(ingredient => res.status(201).send(ingredient))
            .catch(error => res.status(500).send(error));
    },
    getByTypeSup(req,res){
        return IngredientUser
            .findAll({
                where :{
                    type: 'sup'
                }
            })
            .then(ingredient => res.status(201).send(ingredient))
            .catch(error => res.status(500).send(error));
    }
};