/**
 * Created by hoangdo on 9/16/2017.
 */

const Shop = require('../../models').shop;
const Contact = require('../../models').contact;
const Item = require('../../models').item;
const ShopItemDetail = require('../../models').shop_item_detail;
const ItemOption = require('../../models').item_option;
const Sequelize = require('sequelize');

module.exports ={
    list(req,res){
        return Shop
            .all()
            .then(shop =>res.status(201).send(shop))
            .catch(error => res.status(500).send(error));
    },
    retrieve(req,res){
        return Shop
            .findById(req.params.shopId ,{
                include:[{
                    // model: Contact,
                    // as: 'contacts',
                    // where: {contactId : Shop.contactId}
                    all: true
                }]
            })
            .then(shop=> res.status(201).send(shop))
            .catch(error => res.status(404).send(error));
    },
    getShopByItemId(req,res){
        // return ItemOption
        //     .findById(req.params.itemOptionId,{
        //
        //         include:[{
        //             all: true
        //         }]
        //     })
        //     // .all()
        //     .then(items => res.status(200).send(items))
        //     .catch(error => res.status(500).send(error));
        return ShopItemDetail.findAll({
            where: {
                itemOptionId: req.params.itemOptionId
            },
            include: [
                {
                    model: Shop
                },
            ]
        })
            .then(items => res.status(200).send(items))
            .catch(err => res.status(500).send(err))


    }
};