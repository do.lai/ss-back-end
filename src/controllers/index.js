/**
 * Created by hoangdo on 9/9/2017.
 */
const ItemCat = require('./item-cat.controller');
const Item = require('./item.controller');
const ItemOption = require('./item-option.controller');
const  Ingredient = require('./ingredient.controller');
const Contact = require('./contact.controller');
const User = require('./User/user.controller');

const IngredOption = require('./ingred-option.controller');

//****SHOP
const Shop = require('./Shop/shop.controller');
const ShopItemDetail = require('./Shop/shop-item-details.controller');
const IngredientOfShop = require('./Shop/ingredient.controller');
const ShopItem = require('./Shop/shop-item.controller');
//****USER
const Account = require('./User/account.controller');
const ItemUser = require('./User/drink.controller');
const ShopUser = require('./User/shop.controller');
const Order = require('./User/order.controller');
const IngredientUser =require('./User/ingredient.controller');
const ItemOpIngredDetail = require('./User/item-op-ingred-detail.controller');


module.exports = {
    ItemCat,
    Item,
    ItemOption,
    Ingredient,
    Contact,
    User,
    Shop,
    IngredOption,
    IngredientOfShop,
    Account,
    ItemUser,
    ShopItemDetail,
    ShopItem,
    ShopUser,
    Order,
    IngredientUser,
    ItemOpIngredDetail
}