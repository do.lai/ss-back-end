/**
 * Created by hoangdo on 9/15/2017.
 */

const Shop = require('../../models/index').shop;
const Contact = require('../../models/index').contact;
const ShopItemDetail = require('../../models').shop_item_detail;

module.exports ={
    create(req,res){
        return Shop
            .create({
                name: req.body.name,
                description: req.body.description,
                contactId: req.body.contactId
            })
            .then(shop => res.status(201).send(shop))
            .catch(error => res.status(500).send(error));
    },
    list(req, res){
        return Shop
            .all()
            .then(shop => res.status(201).send(shop))
            .catch(error => res.status(500).send(error));
    },
    retrieve(req, res){
        return Shop
            .findById(req.params.shopId , {
                include: [{
                    model: ShopItemDetail,
                    as: 'shop_item_details'
                }]
            })
            .then(shop =>{
                if(!shop){
                    return res.status(404).send({
                        message: 'Shop Not Found'
                    })
                }
                return res.status(200).send(shop);
            })
            .catch(error => res.status(500).send(error));

    },
    getShopByID(req,res){
        return Shop
            .findById(req.params.shopId , {
                include: [{
                   model:Contact
                }]
            })
            .then(shop =>{
                if(!shop){
                    return res.status(404).send({
                        message: 'Shop Not Found'
                    })
                }
                return res.status(200).send(shop);
            })
            .catch(error => res.status(500).send(error));
    },
    update(req,res){
        return Shop
            .findById(req.params.shopId)
            .then(shop => {
                if(!shop){
                    return res.status(404).send({message : 'not found'})
                }
                return shop
                    .update({
                        name: req.body.name || shop.name,
                        description: req.body.description ||shop.description,

                    }).then(shop1 =>{
                        return Contact

                            .update({
                                contactId: shop1.id,
                                email : req.body.email,
                                email2: req.body.email2,
                                name: req.body.name,
                                name2: req.body.name2,
                                hotline: req.body.hotline,
                                office_phone: req.body.office_phone,
                                office_phone2: req.body.office_phone2
                            })

                    })

            })
    }

};