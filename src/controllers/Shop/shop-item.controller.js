/**
 * Created by hoangdo on 9/16/2017.
 */
const ShopItem = require('../../models').item;
const ItemOption = require('../../models').item_option;
const ShopItemDetail = require('../../models').shop_item_detail;
const Shop = require('../../models').shop;

module.exports = {
    create(req,res){
        return ShopItem
            .create({
                name: req.body.name,
                description : req.body.description,
                photo : req.body.photo,
                photo2: req.body.photo2,
                photo3:req.body.photo3,
                photo4: req.body.photo4,
                photo5: req.body.photo5,
                photo6: req.body.photo6,
                amount: req.body.amount,
                unit: req.body.unit,
                status: req.body.status,

            })
            .then(item => res.status(201).send(item))
            .catch(error => res.status(400).send(error));
    },

    list(req,res){
        return ShopItem
        // .findAll({
        //     include: [{
        //         model: ItemOption,
        //         as: 'item_options'
        //     }],
        // })
            .all()
            .then(item =>res.status(201).send(item))
            .catch(err =>res.status(400).send(err));
    },
    retrieve(req,res){
        return ShopItem
            .findById(req.params.itemId ,{

                include: [{
                    model: ItemOption,
                    as: 'item_options'
                }],
            })
            .then(item =>{
                if(!item){
                    return res.status(404).send({
                        message: 'Item not found'
                    });
                }
                return res.status(200).send(item);
            })
            .catch(error => res.status(400).send(error));
    },
    // update(req, res){
    //     return ShopItem
    //         .findById(req.params.itemId ,{
    //             include: [{
    //                 model: ItemOption,
    //                 as: 'item_options',
    //             }],
    //         })
    //         .then(item =>{
    //             if(!item){
    //                 return res.status(404).send({
    //                     message: 'Item Not Found'
    //                 });
    //             }
    //             return item
    //                 .update({
    //                     name: req.body.name || item.name ,
    //                     description: req.body.description  || item.description,
    //                     photo : req.body.photo || item.photo,
    //                     photo2: req.body.photo2 || item.photo2,
    //                     photo3: req.body.photo3 || item.photo3,
    //                     photo4: req.body.photo4 || item.photo4,
    //                     photo5: req.body.photo5 || item.photo5,
    //                     photo6: req.body.photo6  || item.photo6,
    //                     amount: req.body.amount || item.amount,
    //                     unit: req.body.unit || item.unit,
    //                     status: req.body.status || item.status,
    //
    //                 })
    //                 .then(() => res.status(200).send(item))
    //                 .catch(error => res.status(400).send(error));
    //         })
    //         .catch(error =>res.status(400).send(error));
    // },
    destroy(req, res){
        return ShopItemDetail
            .findById(req.params.shopItemId)
            .then(item => {
                if(!item){
                    return res.status(400).send({
                        message: 'Item not found'
                    });
                }
                return item
                    .destroy()
                    .then(()=> res.status(204).send({
                        message: 'Item Delete Successfully.'
                    }))
                    .catch(error => res.status(400).send(error));
            })
            .catch(error => res.status(400).send(error));

    },
    async createItem(req,res){
        let item =  await ShopItem.create({
                name: req.body.local_item_name,
                description : req.body.local_item_desc,
                photo : req.body.photo,
                photo2: req.body.photo2,
                photo3:req.body.photo3,
                photo4: req.body.photo4,
                photo5: req.body.photo5,
                photo6: req.body.photo6,
                amount: req.body.amount,
                unit: req.body.unit,
                status: req.body.status,
            });
        let itemOption = await ItemOption
            .create({
                itemId: parseInt(item.id),
                description: req.body.description,
                type: req.body.type,
                order_count: req.body.order_count,
                status: req.body.status
            });
        const insertObject = {
            itemOptionItemId: parseInt(itemOption.itemId),
            local_item_name: req.body.local_item_name,
            local_item_code: req.body.local_item_code,
            local_item_desc: req.body.local_item_desc,
            item_price: req.body.item_price,
            item_unit: req.body.item_unit,
            local_photo: req.body.local_photo,
            local_photo2: req.body.local_photo2,
            local_photo3: req.body.local_photo3,
            local_photo4: req.body.local_photo4,
            local_photo5: req.body.local_photo5,
            shopId: req.params.shopId,
            itemOptionId: itemOption.id,
        }
        let shopItemDetail = await ShopItemDetail
            .create(insertObject);
        return res.status(200).send(shopItemDetail);

    },
    update(req,res){
        return ShopItemDetail
            .findById(req.params.shopItemId)
            .then(shopitem =>{
                if(!shopitem){
                    return res.status(404).send({
                        message: 'Item not found'
                    })
                }
                return shopitem
                    .update({
                        local_item_name: req.body.local_item_name || shopitem.local_item_name,
                        local_item_code: req.body.local_item_code || shopitem.local_item_code,
                        local_item_desc: req.body.local_item_desc || shopitem.local_item_desc,
                        item_price: req.body.item_price || shopitem.item_price,
                        item_unit: req.body.item_unit || shopitem.item_unit,
                        local_photo: req.body.local_photo || shopitem.local_photo,
                        local_photo2: req.body.local_photo2 || shopitem.local_photo2,
                        local_photo3: req.body.local_photo3 || shopitem.local_photo3,
                        local_photo4: req.body.local_photo4 || shopitem.local_photo4,
                        local_photo5: req.body.local_photo5 || shopitem.local_photo5,
                        shopId: req.params.shopId ,
                        itemOptionId: req.body.itemOptionId || shopitem.itemOptionId,
                        itemOptionItemId: req.body.itemOptionItemId || shopitem.itemOptionItemId
                    })
                    .then(item=>res.status(200).send(item))
                    .catch(error => res.status(500).send(error));
            })
    },
    getShopItemById(req,res){
        return ShopItemDetail
            .findById(req.params.shopItemId)
            .then(item =>res.status(201).send(item))
            .catch(error =>res.status(404).send(error));
    }
};