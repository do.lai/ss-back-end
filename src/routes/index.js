/**
 * Created by hoangdo on 9/9/2017.
 */
const ItemCatCtrl = require('../controllers/index').ItemCat;
const ItemCtrl = require('../controllers/index').Item;
const ItemOptionCtrl = require('../controllers/index').ItemOption;
const IngredientCtrl = require('../controllers/index').Ingredient;
const  ContactCtrl = require('../controllers/index').Contact;
const UserCtrl = require('../controllers/index').User;
const ShopCtrl = require('../controllers/index').Shop;
const IngredOptionCtrl = require('../controllers/index').IngredOption;
const IngredientOfShopCtrl = require('../controllers/index').IngredientOfShop;
//**USER
const AccountCtrl = require('../controllers/index').Account;
const DrinkCtrl = require('../controllers/index').ItemUser;
const ShopUserCtrl = require('../controllers/index').ShopUser;
const OrderCtrl = require('../controllers/index').Order;
const IngredientUserCtrl = require('../controllers/index').IngredientUser;
const ItemOpIngredDetailCtrl = require('../controllers/index').ItemOpIngredDetail;

const ShopItemDetailCtrl = require('../controllers/index').ShopItemDetail;
const ShopItemCtrl = require('../controllers/index').ShopItem;

module.exports = (app) => {
    //item
    app.post('/api/item' ,ItemCtrl.create);
    app.get('/api/shop/item' ,ItemCtrl.list);
    app.get('/api/item/detail/:itemId' ,ItemCtrl.retrieve);
    app.put('/api/item/:itemId' , ItemCtrl.update);
    app.delete('/api/item/:itemId' , ItemCtrl.destroy);

    //item-cat
    app.get('/api/item-cat', ItemCatCtrl.getAll);
    app.post('/api/item-cat', ItemCatCtrl.create);

    //item-option
    app.post('/api/item-option' , ItemOptionCtrl.create);
    app.put('/api/item-option/:itemOptionId' , ItemOptionCtrl.update);
    app.delete('/api/item-option/:itemOptionId' , ItemOptionCtrl.destroy);

    // ingredient
    app.post('/api/ingredient', IngredientCtrl.create);
    app.get('/api/ingredient' ,IngredientCtrl.list);
    app.get('/api/ingredient/:ingredientId' , IngredientCtrl.retrieve);
    app.put('/api/ingredient/:ingredientId' , IngredientCtrl.update);
    app.delete('/api/ingredient/:ingredientId' ,IngredientCtrl.destroy);



    //contact
    app.post('/api/contact' , ContactCtrl.create);
    app.get('/api/contact' , ContactCtrl.list);

    //user
    // app.post('/api/user' , UserCtrl.create);
    // app.get('/api/user' , UserCtrl.list);
    // app.get('/api/user/:userId' , UserCtrl.retrieve);
    app.post('/api/login' , UserCtrl.create);



    //ingredoption
    app.post('/api/ingred-option' , IngredOptionCtrl.create);
    app.put('/api/ingred-option/:ingredOptionId' , IngredOptionCtrl.update);
    app.delete('/api/ingred-option/:ingredOptionId' , IngredOptionCtrl.destroy);

    //**** USer
    //account
    app.post('/api/user/Account' , AccountCtrl.create);
    app.get('/api/user/Account' , AccountCtrl.list);
    app.get('/api/user/Account/:userId' , AccountCtrl.retrieve);
    //Drink
    app.get('/api/user/drink' ,DrinkCtrl.list);
    app.get('/api/user/drink/:itemId' ,DrinkCtrl.retrieve);
    //shop
    app.get('/api/user/shop', ShopUserCtrl.list);
    app.get('/api/user/shop/:shopId' ,ShopUserCtrl.retrieve);
    app.get('/api/user/shop/drink/:itemOptionId' ,ShopUserCtrl.getShopByItemId);
    //order
    app.post('/api/user/:userId/order' ,OrderCtrl.create );
    app.put('/api/user/:userId/order/:orderId' , OrderCtrl.update);
    //Ingredient
    // app.get('/api/user/ingredient',IngredientUserCtrl.list);
    app.get('/api/user/ingredient/sub',IngredientUserCtrl.getByTypeSup);
    app.get('/api/user/ingredient/main',IngredientUserCtrl.getByTypeMain);
    app.get('/api/user/ingredient/:ingredientId' , IngredientUserCtrl.retrieve);
    //itemopingredDetail
    app.post('/api/itemopingred', ItemOpIngredDetailCtrl.create);
    app.get('/api/itemopingred', ItemOpIngredDetailCtrl.getAll);

    //***SHOP
    //shoptiem detail
    app.post('/api/shop/shopitem', ShopItemDetailCtrl.create);
    app.get('/api/shop/shopitem' , ShopItemDetailCtrl.list);
    //ingredientOfShop
    app.post('/api/shop/ingredient', IngredientOfShopCtrl.create);
    app.get('/api/shop/ingredient' ,IngredientOfShopCtrl.list);
    app.get('/api/shop/ingredient/:ingredientId' , IngredientOfShopCtrl.retrieve);
    app.put('/api/shop/ingredient/:ingredientId' , IngredientOfShopCtrl.update);
    app.delete('/api/shop/ingredient/:ingredientId' ,IngredientOfShopCtrl.destroy);
    //shop
    app.post('/api/shop' , ShopCtrl.create);
    app.get('/api/shop', ShopCtrl.list);
    app.get('/api/shop/:shopId',ShopCtrl.getShopByID);
    app.get('/api/shop/:shopId/item' , ShopCtrl.retrieve);

    //shopitem
    app.post('/api/shop/item' ,ShopItemCtrl.create);
    // app.get('/api/shop/item' , ShopItemCtrl.list);
    app.get('/api/shop/item/:itemId' ,ShopItemCtrl.retrieve);
    app.put('/api/shop/item/:itemId' , ShopItemCtrl.update);
    app.delete('/api/shop/item/:itemId' , ShopItemCtrl.destroy);
    app.post('/api/shop/:shopId/item', ShopItemCtrl.createItem);
    app.get('/api/shop/:shopId/item/:shopItemId' , ShopItemCtrl.getShopItemById);
    app.delete('/api/shop/:shopId/item/:shopItemId' ,ShopItemCtrl.destroy );
    app.put('/api/shop/:shopId/item/:shopItemId' ,ShopItemCtrl.update);



}