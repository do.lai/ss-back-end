'use strict';
module.exports = function(sequelize, DataTypes) {
  var user = sequelize.define('user', {
      username: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true,
          validate: {
              is: /^[a-z0-9\_\-]+$/i,
          }
      },
    password: DataTypes.STRING,
    googleID: DataTypes.STRING,
    facebookID: DataTypes.STRING,
    avatar: DataTypes.STRING,
    type: DataTypes.STRING,
    role: DataTypes.STRING
  });
      user.associate = (models)=>{
          user.hasMany(models.order,{
              foreignKey: 'userId',
              as: 'orders'
          });
          user.belongsTo(models.contact ,{
              foreignKey: 'contactId'
          });
      };

  return user;
};