'use strict';
module.exports = function(sequelize, DataTypes) {
  var item_option = sequelize.define('item_option', {
    description: DataTypes.TEXT,
    type: DataTypes.STRING,
    order_count: DataTypes.INTEGER,
    status: DataTypes.STRING,
      ingedList: DataTypes.ARRAY(DataTypes.INTEGER),
      ingredOpList: DataTypes.ARRAY(DataTypes.INTEGER)
  });
    item_option.associate = (models)=> {
        item_option.belongsTo(models.item ,{
            foreignKey: 'itemId'

        });
        item_option.hasMany(models.shop_item_detail,{
            foreignKey: 'itemOptionId',
            as: 'shop_item_details'
        });
        item_option.hasMany(models.item_op_ingred_detail,{
            foreignKey: 'itemOptionId',
            as: 'item_op_ingred_details'
        });
    };
  return item_option;
};