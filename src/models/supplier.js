'use strict';
module.exports = function(sequelize, DataTypes) {
  var supplier = sequelize.define('supplier', {
    name: DataTypes.STRING,
    compID: DataTypes.STRING
  });
  supplier.associate =(models) =>{
    supplier.belongsTo(models.contact ,{
      foreignKey: 'contactId'
    });
    supplier.hasMany(models.supplier_ingred_op_detail ,{
      foreignKey: 'supplierId',
        as: 'supplier_ingred_op_details'
    });
  };
  return supplier;
};