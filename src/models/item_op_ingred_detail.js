'use strict';
module.exports = function(sequelize, DataTypes) {
  var item_op_ingred_detail = sequelize.define('item_op_ingred_detail', {
    amount: DataTypes.DECIMAL,
    amount_type: DataTypes.STRING,
    unit: DataTypes.STRING
  });
  item_op_ingred_detail.associate = (models) =>{
    item_op_ingred_detail.belongsTo(models.item_option ,{
      foreignKey: 'itemOptionId'
    });
    item_op_ingred_detail.belongsTo(models.ingred_option ,{
      foreignKey: 'ingredOptionId'
    });
  };
  return item_op_ingred_detail;
};