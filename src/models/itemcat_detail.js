'use strict';
module.exports = function(sequelize, DataTypes) {
  var itemCat_detail = sequelize.define('itemCat_detail', {
    item_iditem: DataTypes.INTEGER
  });
    itemCat_detail.associate = (models)=> {
        itemCat_detail.belongsTo(models.item_cat ,{
            foreignKey: 'itemCatId'

        });
           itemCat_detail.belongsTo(models.item ,{
             foreignKey: 'itemId'

           });
    };
  return itemCat_detail;
};