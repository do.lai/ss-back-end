'use strict';
module.exports = function(sequelize, DataTypes) {
  var item = sequelize.define('item', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    photo: DataTypes.STRING,
    photo2: DataTypes.STRING,
    photo3: DataTypes.STRING,
    photo4: DataTypes.STRING,
    photo5: DataTypes.STRING,
    photo6: DataTypes.STRING,
    amount: DataTypes.DECIMAL,
    unit: DataTypes.STRING,
    status: DataTypes.STRING
  });
    item.associate = (models)=>{
        item.hasMany(models.itemCat_detail ,{
            foreignKey: 'itemId',
            as: 'itemCat_details'
        });
        item.hasMany(models.item_option ,{
          foreignKey: 'itemId',
            as: 'item_options'
        });
    };
  return item;
};