'use strict';
module.exports = function(sequelize, DataTypes) {
  var ingredient = sequelize.define('ingredient', {
    name: DataTypes.STRING,
    photo: DataTypes.STRING,
    description: DataTypes.TEXT,
    source_type: DataTypes.STRING,
    process_type: DataTypes.STRING,
    status: DataTypes.STRING,
      type: DataTypes.STRING
  });
  ingredient.associate = (models) =>{
    ingredient.belongsToMany(models.ingred_cat , {through : 'ingredCat_detail' , foreignKey : 'ingredientId'} );
    ingredient.hasMany(models.ingred_option ,{
      foreignKey: 'ingredientId',
        as: 'ingred_options'
    });
  };
  return ingredient;
};