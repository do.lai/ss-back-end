'use strict';
module.exports = function(sequelize, DataTypes) {
  var supplier_ingred_op_detail = sequelize.define('supplier_ingred_op_detail', {
    pirce: DataTypes.DECIMAL,
    unit: DataTypes.STRING,
    ship_fee: DataTypes.DECIMAL,
    sku_code: DataTypes.STRING,
    sku_name: DataTypes.STRING,
    description: DataTypes.TEXT,
    best_before: DataTypes.DATE,
    expired_after: DataTypes.DECIMAL,
    expired_type: DataTypes.STRING,
    expired_unit: DataTypes.STRING,
    origin: DataTypes.STRING,
    licence_link: DataTypes.TEXT,
    lisence_link2: DataTypes.TEXT,
    lisence_link3: DataTypes.TEXT,
    lisence_link4: DataTypes.TEXT,
    lisence_link5: DataTypes.TEXT,
    status: DataTypes.STRING
  });
  supplier_ingred_op_detail.associate =(models) =>{
    supplier_ingred_op_detail.belongsTo(models.ingred_option , {
      foreignKey: 'ingredOptionId'
    });
    supplier_ingred_op_detail.belongsTo(models.supplier ,{
      foreignKey: 'supplierId'
    });
  };
  return supplier_ingred_op_detail;
};