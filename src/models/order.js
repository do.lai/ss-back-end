'use strict';
module.exports = function(sequelize, DataTypes) {
  var order = sequelize.define('order', {
    notes: DataTypes.TEXT,
    address: DataTypes.TEXT,
    mobile: DataTypes.STRING,
    mobile2: DataTypes.STRING,
    cash_type: DataTypes.STRING,
    total: DataTypes.DECIMAL,
    sub_total: DataTypes.DECIMAL,
    tax: DataTypes.DECIMAL,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    ship: DataTypes.DECIMAL,
    trackingNumber: DataTypes.STRING,
    status: DataTypes.STRING
  });
    order.associate = (models)=> {
        order.belongsTo(models.user ,{
            foreignKey: 'userId',
            onDelete: 'CASCADE'
        });
        order.belongsTo(models.payment ,{
            foreignKey: 'paymentId'

        });
        order.hasMany(models.order_detail,{
          foreignKey: 'orderId',
            as: 'order_details'
        });
    };

  return order;
};