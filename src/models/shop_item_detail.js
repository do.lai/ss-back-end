'use strict';
module.exports = function(sequelize, DataTypes) {
  var shop_item_detail = sequelize.define('shop_item_detail', {
    local_item_name: DataTypes.STRING,
    local_item_code: DataTypes.STRING,
    local_item_desc: DataTypes.STRING,
    item_price: DataTypes.DECIMAL,
    item_unit: DataTypes.STRING,
    local_photo: DataTypes.STRING,
    local_photo2: DataTypes.STRING,
    local_photo3: DataTypes.STRING,
    local_photo4: DataTypes.STRING,
    local_photo5: DataTypes.STRING
  });
    shop_item_detail.associate = (models)=>{
        shop_item_detail.hasMany(models.order_detail,{
            foreignKey: 'shopItemId',
            as: 'order_details'
        });
        shop_item_detail.belongsTo(models.shop ,{
            foreignKey: 'shopId'
        });
        shop_item_detail.belongsTo(models.item_option,{
            foreignKey:'itemOptionId'
        });
    };
  return shop_item_detail;
};