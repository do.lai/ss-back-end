'use strict';
module.exports = function(sequelize, DataTypes) {
  var ingred_cat = sequelize.define('ingred_cat', {
    name: DataTypes.STRING,
    photo: DataTypes.STRING
  });
  ingred_cat.associate =(models) =>{
    ingred_cat.belongsToMany(models.ingredient , {through: 'ingredCat_detail', foreignKey: 'ingredCatId' });
  };
  return ingred_cat;
};