'use strict';
module.exports = function(sequelize, DataTypes) {
  var contact = sequelize.define('contact', {
      email:{
          type:  DataTypes.STRING,
          validate: {
              isEmail: true
          }
      },
      email2:{
          type:  DataTypes.STRING,
          validate: {
              isEmail: true
          }
      },
    name: DataTypes.STRING,
    name2: DataTypes.STRING,
    hotline: DataTypes.STRING,
    office_phone: DataTypes.STRING,
    office_phone2: DataTypes.STRING,
      latitude: DataTypes.DECIMAL(9, 6),
      longitude: DataTypes.DECIMAL(9, 6)
  });
  contact.associate =(models) =>{
    contact.hasOne(models.shop ,{
      foreignKey: 'contactId',
        as: 'shops'
    });
    contact.hasOne(models.supplier , {
      foreignKey: 'contactId',
        as: 'suppliers'
    });
      contact.hasOne(models.user,{
          foreignKey: 'contactId',
          as: 'users'
      });
  };
  return contact;
};