/**
 * Created by hoangdo on 9/9/2017.
 */

import express from  'express';
import logger from 'morgan';
import bodyParser from 'body-parser';
import multer from 'multer';
// const express= require('express');
// const logger = require('morgan') ;
// const bodyParser = require('body-parser') ;
// const multer = require('multer');
const app = express();
var upload = multer();



app.use(logger('dev'));

// for parsing application/json
app.use(bodyParser.json());
// for parsing application/xwww-
app.use(bodyParser.urlencoded({extended: false}));

// for parsing multipart/form-data
app.use(upload.array());
// app.use(express.static('public'));

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*' );

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

require('./routes/index')(app);

app.get('*', (req, res) => {
    return res.status(200).send({
        message: 'Doll '
    });
});

module.exports = app;